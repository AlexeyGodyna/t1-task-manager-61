package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId);

    void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId);

}
